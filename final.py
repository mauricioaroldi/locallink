#!/usr/bin/python

#Firmware v1.2
#LCD 16x2, RFID 13,56MHz, RTC, 2 botoes

import RPi.GPIO as GPIO
from Adafruit_CharLCD import Adafruit_CharLCD
from subprocess import *
from time import sleep, strftime
from datetime import datetime
import urllib2 
import MFRC522
import signal
import string

lcd = Adafruit_CharLCD()
lcd.begin(16, 2)

update = False
MACadd = ''
continue_reading = True
version = '1.2'
off = False

timerL2 = 61.0

GPIO.setmode(GPIO.BOARD)
but1 = 26 #entrada
but2 = 7 #saida
buz = 10
GPIO.setup(but1, GPIO.IN)
GPIO.setup(but2, GPIO.IN)
GPIO.setup(buz, GPIO.OUT)
situationBtn = ''  #entrada,  saida

def buzzer(pitch, duration):
	period = 1.0 / pitch
	delay = period / 2
	cycles = int(duration * pitch)
	for i in range(cycles):
		GPIO.output(buz, True)
		sleep(delay)
		GPIO.output(buz, False)
		sleep(delay)
        
def checkConnection():
    try:
        urllib2.urlopen("http://www.google.com").close()
    except urllib2.URLError:
        return False
    else:
        return True
        
def readButton(but1, but2):
    global situationBtn
    state1 = GPIO.input(but1)
    state2 = GPIO.input(but2)
    if state1:
        situationBtn = 'enter'
        #acender LED do botao de entrada
    elif state2:
        situationBtn = 'leave'
        #acender LED do botao de saida

def end_read(signal, frame):
	global continue_reading
	print 'Ctrl+C captured, ending read'
	continue_reading = False
	GPIO.cleanup()
        
def readRFID(situation,mac):
    lcd.setCursor(0,1)
    lcd.message("Passe seu cartao")
    (status,TagType) = MIFAREReader.MFRC522_Request(MIFAREReader.PICC_REQIDL)
    code = ''
    
    if status == MIFAREReader.MI_OK:
        print "Card detected"
        buzzer(2000, 0.1)
    (status,uid) = MIFAREReader.MFRC522_Anticoll()

    if status == MIFAREReader.MI_OK:
        # Print UID
        #print "Card read UID: "+str(uid[0])+","+str(uid[1])+","+str(uid[2])+","+str(uid[3])
    	for i in range(len(uid)):
	    code += format(uid[i], '02x')
        # This is the default key for authentication
        key = [0xFF,0xFF,0xFF,0xFF,0xFF,0xFF]

        if status == MIFAREReader.MI_OK:
            MIFAREReader.MFRC522_StopCrypto1()
            sendData(code, situation,mac)
        else:
            print "Authentication error"
            buzzer(1000, 0.2)
            buzzer(5000, 0.2)
        
        
def sendData(tags, situation,mac):
    global timerL2
    global situationBtn, tag, off
    lcd.setCursor(0,1)
    lcd.message('Aguarde         ')
    data = 'curl -m 15 -d "id='+tags+'&time='+datetime.now().strftime('%Y/%m/%d_%H:%M:%S')+'&type='+situation+'&mac='+mac+'" http://www.remotelink.com.br/HW/put.php'
    isConnected = checkConnection()
    if(isConnected == False):
        lcd.setCursor(0,1)
        lcd.message('Dado registrado')
        buzzer(2000, 0.1)
        buzzer(2000, 0.1)
        timerL2 = timer()
        f = open('/home/pi/via/offline.sh', 'a')
        f.write(data)
        f.write('\n')
        f.close()
        off = True
	situationBtn = '' 
    else:
        try:
            p2 = Popen( data, shell=True, stdout=PIPE, close_fds=True)
            output =  p2.communicate()
        except pycurl.error:
            lcd.setCursor(0,1)
            lcd.message('Dado registrado')
            buzzer(2000, 0.1)
            buzzer(2000, 0.1)
            timerL2 = timer()
            f = open('/home/pi/via/offline.sh', 'a')
            f.write(data)
            f.write('\n')
            f.close()
            situationBtn = ''
            off = True
        else:
            f = open('/home/pi/via/arquivo.sh', 'a')
            f.write(data)
            f.write('\n')
            f.close()
            lcd.setCursor(0,1)
            lcd.message(output[0])
            timerL2 = timer()
            situationBtn = ''
            tag = ''
            
               
def sendOffline():
    global timerL2, off, situationBtn
    lcd.setCursor(0,1)
    lcd.message('Enviando dados..')
    timerL2 = timer()
    p1 = Popen(['./via/offline.sh', '-t 30'], shell=True, stdout=PIPE, close_fds=True)
    output = p1.communicate()[0]
    if output == " " or output == 'Entrada repetida.':
        off = False
    else:
        run_cmd('cat /home/pi/via/offline.sh >> /home/pi/via/arquivo.sh')
        run_cmd('> /home/pi/via/offline.sh')
        f = open('/home/pi/via/offline.sh', 'w')
        f.write('#!/bin/sh\n')
        f.close()
        situationBtn = ''
        off = False
    
def device(MACadd):
    global update
    global version
    if timer2() == 0 or timer2() == 15 or timer2() == 30 or timer2() == 45:
        lcd.setCursor(0,1)
        lcd.message('Aguarde: update ')
        dev = 'curl -m 15 -d "time='+datetime.now().strftime('%Y/%m/%d_%H:%M:%S')+'&type='+MACadd+'&version='+version+'" http://www.remotelink.com.br/HW/device.php'
        p3 = Popen(dev, shell=True, stdout=PIPE, close_fds=True)
        output = p3.communicate()[0]
        if output == 'Atualizado.':
            update = False
        print output
	lcd.setCursor(0,1)
        lcd.message('                    ')
        
    
def run_cmd(cmd):
    p = Popen(cmd, shell=True, stdout=PIPE)
    output = p.communicate()[0]
    return output
    
def run_cmdIP():
    global MACadd
    p = Popen("ip addr show eth0 | grep inet | awk '{print $2}' | cut -d/ -f1", shell=True, stdout=PIPE)
    MACadd = open('/sys/class/net/eth0/address').read()[:-1]
    output = p.communicate()[0]
    if output == '':
        p = Popen("ip addr show wlan0 | grep inet | awk '{print $2}' | cut -d/ -f1", shell=True, stdout=PIPE)
        MACadd = open('/sys/class/net/wlan0/address').read()[:-1]
        output = p.communicate()[0]
    return output
    
def timer():
    return int( datetime.now().strftime('%M'))+(int( datetime.now().strftime('%S')) * 0.01)

def timer2():
    return int(datetime.now().strftime('%M'))
######################################################        
run_cmd('cat /home/pi/via/offline.sh >> /home/pi/via/arquivo.sh')       

IPadd = run_cmdIP()
MACprint = MACadd.translate(string.maketrans("",""), string.punctuation)
        
lcd.clear()
lcd.setCursor(0,0)
lcd.message("Inicializando...")
lcd.setCursor(0,1)
lcd.message("MAC %s" % (MACprint))
sleep(10)
lcd.setCursor(0,1)
lcd.message("IP %s" % (IPadd))
sleep(10)  
lcd.clear()

signal.signal(signal.SIGINT, end_read)
MIFAREReader = MFRC522.MFRC522()

while True:
    #Main
    lcd.setCursor(0,0)
    lcd.message(datetime.now().strftime('%H:%M %d/%m/%Y'))
    if off and checkConnection():
        sendOffline()
    
    readButton(but1, but2)
    if situationBtn:
        readRFID(situationBtn,MACadd) 
        
    if timer2() == 1 or timer2() == 16 or timer2() == 31 or timer2() == 46:
        update = False
    if timer2() == 59 or timer2() == 14 or timer2() == 29 or timer2() == 44:
        update = True

    if update:    
        device(MACadd)    
    
    if timerL2 < 61.0:
        if timer() >= timerL2 + 0.05:
            lcd.clear()
            timerL2 = 61.0
        
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
